from os import fork, getpid, popen, kill, fork, system
from time import sleep
from getpass import getuser
import signal
from select import select
import psutil
import sys
import tty, termios
from multiprocessing import Value, Process, Condition
import threading

class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        self.impl = _GetchUnix()

    def __call__(self, timeout=-1): return self.impl(timeout)


class _GetchUnix:
    def __init__(self):
        import tty

    def __call__(self, timeout=-1):
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            if timeout > 0:
                rlist, _, _ = select([sys.stdin], [], [], timeout)
                if not rlist:
                    return None
            ch = sys.stdin.read(1)
            if ch in 'qkc':
                print(ch, end='')
            sys.stdout.flush()
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

getch = _Getch()

myself = getpid()
current_user = getuser()

class Column:
    def __init__(self, title, align, length, info):
        self.title = title
        self.align = align
        self.length = length
        self.info = info

is_running = lambda p: p.status() not in [psutil.STATUS_ZOMBIE, psutil.STATUS_DEAD, psutil.STATUS_SLEEPING]
is_it_me = lambda pid: pid == myself
is_my_child = lambda p: is_it_me(p.ppid()) and p.status() != psutil.STATUS_ZOMBIE
has_cmd = lambda p: len(p.cmdline())
center_align = lambda s, length: s.center(length)
right_align = lambda s, length: s.rjust(length)
left_align = lambda s, length: s.ljust(length)
project = lambda projection, cols: lambda data=None: '|'.join([projection(c)(data) for c in cols])
dashes = lambda col_spec: lambda data=None: '-' * col_spec.length
titles = lambda col_spec: lambda data=None: adjust_length(col_spec.title, col_spec.align, col_spec.length)
info = lambda col_spec: lambda data=None: adjust_length(col_spec.info(data), col_spec.align, col_spec.length)


def adjust_length(string, align, length):
    if len(string) > length - 2: return f' {string[:length - 5]}... '
    else: return f' {align(string, length - 2)} '

def get_term_with():
    try:
        _, columns = popen('stty size', 'r').read().split()
        return int(columns)
    except:
        return 100

def display_process():
    cols = [
        Column('PID'    , right_align, 7 , lambda p: str(p.pid)),
        Column('Name', left_align , min(32, get_term_with() - 8 - 20 - 3), lambda p: p.name() + (' [me]' if is_it_me(p.pid) else '')),
        Column('User'   , left_align , 20 + 2, lambda p: p.username())
    ]
    print(project(titles, cols)())
    print(project(dashes, cols)())
    interest = lambda p: is_my_child(p) or is_running(p) and has_cmd(p)
    processes = map(project(info, cols), filter(interest, psutil.process_iter()))
    for p in processes: print(p)
    print()

def kill_process(pid):
    try:
        pid = int(pid)
        kill(pid, signal.SIGTERM)
        return True
    except:
        return False


def dummy_process(children_count, lock, running, cv):
    with lock: children_count.value += 1
    for _ in range(300):
        with cv:
            if not running.value: break
        with lock:
            lock.wait(.1)

    with lock: children_count.value -= 1

count = Value('i', 0)
children_lock = Condition()
running = Value('i', 1)
cv = Condition()
children = []

def create_processes(how_many):
    for _ in range(how_many):
        p = Process(target=dummy_process, args=(count, children_lock, running, cv))
        p.start()
        with children_lock: children.append(p)

def spawn_processes(running, cv):
    while True:
        with cv:
            if not running.value: break
            with children_lock:
                how_many = count.value or 1
            create_processes(how_many)
            cv.wait(10)

spawn_thread = threading.Thread(target=spawn_processes, args=(running, cv))
spawn_thread.start()
message='Press `q\' to exit or `k <PID> + return\' to terminate some process `c <N> + return\' to create N processes: '
timeout=1
while True:
    print()
    display_process()
    print(message, end='')
    sys.stdout.flush()
    c = 'z'
    while c != 'q':
        c = getch(timeout=1)
        if c == None:
            break
        elif c == 'k':
            pid = sys.stdin.readline().strip()
            kill_process(pid)
            break
        elif c == 'c':
            how_many = sys.stdin.readline().strip()
            try:
                how_many = int(how_many)
                create_processes(how_many)
            except:
                pass
            break
    if c == 'q':
      print()
      break


with cv:
    running.value = 0
    cv.notify_all()

spawn_thread.join()

for p in children:
    if p.is_alive(): p.terminate()

